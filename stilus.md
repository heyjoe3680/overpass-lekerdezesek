Sokat segít, ha a megjelenített adatainkat vízuálisan is testre szabhatjuk. Erre a [MapCSS](https://wiki.openstreetmap.org/wiki/MapCSS/0.2) van segítségünkre. A különbző stílusfajták használatát most nem taglalom, a referenciák közt megtalálható a különböző paraméterek felvehető értékei.

Szintaxisa:

    ... overpass lekérdezés ...
    {{style:
    ... mapcss stílus ...
    }}

Kezdjük egy egyszerű példával:

    [out:json][timeout:25];
    {{geocodeArea:"Káld"}}->.searchArea;
    (  
     nwr["checkpoint"="hiking"](area.searchArea);
    );
    out body geom;
    {{style:
      node, way, relation {
        text: name;
      }
    }}
    
[futtatható példa](http://overpass-turbo.eu/s/LqQ)

A lekérdezésünkben nincsen semmi újdonság, (talán csak annyi, hogy nem írtuk ki külön, hogy node, way, és relation elemeken végezzük a keresést, hanem ezt az nwr-rel jeleztük), túramozgalmak igazolópontjait keressük Káldon. A stílusunk sem túl bonyolult, minden pont, vonal, kapcsolatnak mutassa a name címke tartalmát.

Bonyolítsunk kicsit:

    [out:json][timeout:25];
    {{geocodeArea:"Káld"}}->.searchArea;
    (
      relation["jel"="kt"](area.searchArea);
    relation["jel"="k3"](area.searchArea);
    );
    out body geom;
    (
    relation["jel"="kmtb"](area.searchArea);way(r)->.n1;
    );
    way.n1;
    out body geom;
    {{style:
    relation[jel=kt]
    { color:brown; width:4;  }
    relation[jel=k3]
    { color:yellow; width:6; dashes:2,10,2,10; }
    way[highway=track]
    { color:red; width:2;}
    way[surface=asphalt]
    { color:blue; width:3;}
    way[highway=residential]
    { color:black; width:3;}
    }}
    
[futtatható példa](http://overpass-turbo.eu/s/LqT)

Látható a példakódban, hogy két kimenetünk van: az elsőben a tanösvény, és a kék háromszög kapcsolatot kérjük le, míg a másodikban a kék kerékpáros jelzés útjait rakjuk a kimenetbe, így van lehetőségünk színezni a kapcsolat címkéi, a második esetben pedig a kapcsolat újainak címkéi alapján.

A következő példa futtatása előtt lépjünk be az Overpass Turbo Beállítások menüpontjába, a Térkép pont alatt állítsuk az csempe átlátszóságot 0-ra, és tegyünk egy pipát a Ne jelezze a kis méretű elemeket POI-ként pont elé.


    [out:json][timeout:25];
    {{geocodeArea:"Káld"}}->.searchArea;
    (  
     way["building"](area.searchArea);
     way["highway"](area.searchArea);
  
    );
    out body geom;
    (  
     way[amenity=ice_cream](area.searchArea);  
      way[amenity=pub](area.searchArea);
    );
    out center;
    {{style:
    node, area
    { color:black; fill-color:black; }
    way[highway]
     { color:gray ; width:10;}
    area[amenity=place_of_worship]
    { color:red; fill-color:red; }
    area[shop=convenience]
    { color:blue; fill-color:blue; }
    node[amenity=ice_cream] {
      icon-image: url('http://icons.iconarchive.com/icons/google/noto-emoji-food-drink/256/32416-soft-ice-cream-icon.png');
      icon-width: 36;
      color: red;
    }
    node[amenity=pub] {
      icon-image: url('icons/mapnik/pub.png');
      icon-width: 18;
      color: red;
    }
    }}
    
[futtatható példa](http://overpass-turbo.eu/s/LqY)

![Kép](https://bitbucket.org/heyjoe3680/overpass-lekerdezesek/raw/b8bdff4812651bb58734cb5e40d136f8c67d639b/mapcss.png)
